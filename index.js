const Joi = require("joi");
const express = require("express");
const app = express();
var uuid = require("uuid");
var cookieParser = require("cookie-parser");
app.use(cookieParser());
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
var auth = [];
var groups = [];
var signedIn = [];
var notes = [];
app.get("/", (req, res) => {
  res.send("Hello,World");
});

let users = {
  id: "32602bec-5ae1-4698-b335-3cac77065036",
};

//Route for adding cookie
app.get("/setuser", (req, res) => {
  res.cookie("userData", users);
  res.send("user data added to cookie");
});

//Iterate users data from cookie
app.get("/getuser", (req, res) => {
  //shows all the cookies
  res.send(req.cookies);
});
app.get("/api/users", (req, res) => {
  const fs = require("fs");

  let data = fs.readFileSync("users.json");
  let users = JSON.parse(data);
  res.json(users);
});
app.get("/api/signedin", (req, res) => {
  const fs = require("fs");

  let data = fs.readFileSync("signedin.json");
  let auth = JSON.parse(data);
  res.json(auth);
});
app.get("/api/auth", (req, res) => {
  const fs = require("fs");

  let data = fs.readFileSync("input.json");
  let auth = JSON.parse(data);
  res.json(auth);
});
app.get("/api/groups", (req, res) => {
  const fs = require("fs");

  let data = fs.readFileSync("groups.json");
  let group = JSON.parse(data);
  res.json(group);
});
app.get("/api/notes", (req, res) => {
  const fs = require("fs");

  let data = fs.readFileSync("notes.json");
  let note = JSON.parse(data);
  res.json(note);
});
app.get("/api/notifications", (req, res) => {
  const fs = require("fs");

  let data = fs.readFileSync("notifications.json");
  let notifications = JSON.parse(data);
  res.json(notifications);
});
app.get("/api/ads/", (req, res) => {
  const ads = [
    { id: 1, title: "bla 1", content: "bla bla bla" },
    { id: 2, title: "bla 2", content: "bla bla bla" },
    { id: 3, title: "bla 3", content: "bla bla bla" },
  ];
  res.json(courses);
});
app.post("/api/signedin", function (request, response) {
  var member = request.body;

  var fs = require("fs");
  signedIn.push(member);
  fs.writeFile("signedin.json", JSON.stringify(member), function (err) {
    if (err) throw err;
  });

  response.json(groups);
});
app.post("/api/users", function (request, response) {
  console.log("users", request.body.type);
  if (request.body.type == "delete") {
    var member = request.body;
    var fs = require("fs");
    fs.readFile("users.json", "utf8", (err, jsonString) => {
      if (err) {
        return;
      }
      var temp = [];
      var users = JSON.parse(jsonString);
      for (let i = 0; i < users.length; i++) {
        if (
          users[i].userId != request.body.userId &&
          users[i].groupId != request.body.groupId
        ) {
          temp.push(users[i]);
        }
      }
      users = temp;

      fs.writeFile("users.json", JSON.stringify(users), function (err) {
        if (err) throw err;
      });
    });
  } else {
    var member = request.body;
    var fs = require("fs");
    console.log("apo");
    fs.readFile("users.json", "utf8", (err, jsonString) => {
      if (err) {
        return;
      }
      var users = JSON.parse(jsonString);
      users.push(member);

      fs.writeFile("users.json", JSON.stringify(users), function (err) {
        if (err) throw err;
      });
    });
  }

  response.json(users);
});
app.post("/api/notifications", function (request, response) {
  var member = request.body;
  console.log("member", member);
  if (request.body.type === "delete") {
    var fs = require("fs");
    fs.readFile("notifications.json", "utf8", (err, jsonString) => {
      if (err) {
        return;
      }
      var temp = [];
      var notifications = JSON.parse(jsonString);
      console.log("notifications", notifications);
      for (let i = 0; i < notifications.length; i++) {
        if (
          notifications[i].groupId != request.body.groupId &&
          notifications[i].userId != request.body.userId &&
          notifications[i].type == "join"
        ) {
          temp.push(notifications[i]);
        }
      }
      //  notifications.push(member);
      notifications = temp;
      fs.writeFile(
        "notifications.json",
        JSON.stringify(notifications),
        function (err) {
          if (err) throw err;
        }
      );
    });
  } else {
    var fs = require("fs");
    fs.readFile("notifications.json", "utf8", (err, jsonString) => {
      if (err) {
        return;
      }
      var notifications = JSON.parse(jsonString);
      notifications.push(member);

      fs.writeFile(
        "notifications.json",
        JSON.stringify(notifications),
        function (err) {
          if (err) throw err;
        }
      );
    });
  }

  response.json(notifications);
});

app.post("/api/notes", function (request, response) {
  console.log("file", request.body);
  if (request.body.type) {
    if (request.body.type == "delete") {
      const removeArrayItem = (arr, itemToRemove) => {
        return arr.filter((item) => item !== itemToRemove);
      };
      console.log(request.body);
      var fs = require("fs");
      fs.readFile("notes.json", "utf8", (err, jsonString) => {
        if (err) {
          console.log("File read failed:", err);
          return;
        }
        console.log("File data:", jsonString);
        var valid = 0;
        var notes = JSON.parse(jsonString);
        var temp = [];
        for (let i = 0; i < notes.length; i++) {
          if (notes[i][0].groupId === request.body.groupId) {
            if (notes[i][0].id != request.body.noteId) {
              temp.push(notes[i]);
            }
          }
        }
        console.log("array", notes);
        notes = temp;
        fs.writeFile("notes.json", JSON.stringify(notes), function (err) {
          if (err) throw err;
          console.log("complete");
        });
      });
      response.json(notes);
    }
  } else {
    var member = request.body;
    console.log("member", member);
    var fs = require("fs");
    fs.readFile("notes.json", "utf8", (err, jsonString) => {
      if (err) {
        console.log("File read failed:", err);
        return;
      }
      console.log("File data:", jsonString);
      var valid = 0;
      var notes = JSON.parse(jsonString);
      for (let i = 0; i < notes.length; i++) {
        if (notes[i][0].id === member.id) {
          notes[i].push(member);
          valid = -1;
          break;
        }
      }
      console.log("member", member.id);

      if (valid != -1) {
        notes.push(member);
      }
      console.log("array", notes);
      console.log("auth", notes);

      fs.writeFile("notes.json", JSON.stringify(notes), function (err) {
        if (err) throw err;
        console.log("complete");
      });
    });

    response.json(notes);
  }
});

app.post("/api/groups", function (request, response) {
  var member = request.body;
  console.log("member", member);
  var fs = require("fs");
  fs.readFile("groups.json", "utf8", (err, jsonString) => {
    if (err) {
      return;
    }
    var groups = JSON.parse(jsonString);
    groups.push(member);

    fs.writeFile("groups.json", JSON.stringify(groups), function (err) {
      if (err) throw err;
    });
  });

  response.json(groups);
});
app.post("/api/auth", function (request, response) {
  console.log("data", request.body);
  var member = request.body;
  var fs = require("fs");
  fs.readFile("input.json", "utf8", (err, jsonString) => {
    if (err) {
      return;
    }
    var auth = JSON.parse(jsonString);
    auth.push(member);

    fs.writeFile("input.json", JSON.stringify(auth), function (err) {
      if (err) throw err;
    });
  });

  response.json(auth);
});
app.post("/api/courses", (req, res) => {
  const schema = {
    title: Joi.string().min(3).required(),
    content: Joi.string().min(3).required(),
  };
  const result = Joi.validate(req.body, schema);
  if (result.error) {
    res.status(400).send(result.error.details[0].message);
    return;
  }
  const course = {
    id: courses.length + 1,
    title: req.body.title,
    content: req.body.content,
  };
  courses.push(course);
  res.send(course);
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log({ port }));
